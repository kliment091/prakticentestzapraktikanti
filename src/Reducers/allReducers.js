import colReducer from "./colReducer";
import { combineReducers } from "redux";
import rowReducer from "./rowReducer";
import selReducer from "./selReducer"
const allReducers=combineReducers({
    colReducer:colReducer,
    rowReducer:rowReducer,
    selReducer:selReducer

})
export default allReducers;