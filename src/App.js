import './App.css';
import {BrowserRouter as Router,Route,Routes,Switch} from "react-router-dom"
import bootstrap from 'bootstrap';

//components
import Nav from './components/Nav';
import Home from './components/Home';
import Datas from './components/Datas';
import MoreInfo from './components/MoreInfo';



function App() {
  return (
    <Router>
    <div className="App">
 <Nav/>
 {/* router */}
<div className='mid'>
<Routes>
  <Route exact path='/' element={<Home/>}/>
<Route path='/data' element={<Datas/>}/>
<Route path='/moreInfo/:id' element={<MoreInfo/>}/>
</Routes>
</div>
    </div>
    </Router>
  );
} 

export default App;
