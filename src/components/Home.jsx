import React from 'react'
import { useEffect } from 'react'
import axios from 'axios'
import { useDispatch } from 'react-redux'
import { fetchR,fetch,cleanup } from '../Actions'
const Home = () => {
  const dispatch=useDispatch()
  
  useEffect(() => {
    dispatch(cleanup())
    axios.get(`gridConfig.json`).then((response) => {
      dispatch(fetch(response.data))
    });
    axios.get(`gridData.json`).then((response) => {
   dispatch(fetchR(response.data))
    });
 
  }, []);
 

  return (
    <div>Home</div>
  )
}

export default Home