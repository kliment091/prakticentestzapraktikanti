import React, { useState } from 'react'
import { useSelector } from 'react-redux'
import FormContainer from "./FormContainer"
const MoreInfo = () => {
  const data = useSelector((state) => state.selReducer);
  
  // const [data,setData]=useState(dat)


  return (
    <div className='moreinfo'>
{/* IF STATUS VALID POKAZI FORM IF STATUS INVALID SAMO INFO BEZ OPCIJA ZA EDIT */}
<div className='infoBox'>
<p>Full name: {data["ENTITY.NAME"]} {data['ENTITY.ADDITIONAL_NAME']} {data['ENTITY.SURNAME']}</p>
<p>Gender: {data["ENTITY.GENDER"]}  </p>
<p>Date Of Birth: {data['ENTITY.BIRTH_DATE']} </p>
<p>Location info:  {data['ENTITY.LOCATION']} </p>
<p>Address: {data["ENTITY.ADDRESS"]}</p>
<p>Contact : Mob: {data["ENTITY.MOBILE"]}  Home: {data["ENTITY.PHONE"]}</p>
</div>

{(data["ENTITY.STATUS"]=="Valid")?<FormContainer/>:<div><h1><strong>The status of this row is invalid,editing is disabled</strong></h1></div>}

    </div>
  )
}

export default MoreInfo