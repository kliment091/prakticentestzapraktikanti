import React from 'react'
import { Link } from 'react-router-dom'
const Nav = () => {
  return (
    <div className='navBar'>
     <ul>
<Link to="/">
        <li>Home</li>
        </Link>
        <Link to="/data">
        <li>Grid</li>
        </Link>
      </ul>
    
    </div>
  )
}

export default Nav