import React, { useState } from "react";
import ReactDataGrid from "react-data-grid";
import { useSelector,useDispatch } from "react-redux";
import Toolbar from "./Toolbar";
import bootstrap from "bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import { Data,  Filters } from "react-data-grid-addons";
import { selected } from "../Actions";
import { useNavigate } from "react-router-dom";


const { AutoCompleteFilter } = Filters;
const auto =AutoCompleteFilter
const defaultColumnProperties = {
  filterable: true,
  resizable: true,


};
const sortColProperties={
  sortable:true
}

const autoCol={
  filterRenderer:auto
}
const selectors = Data.Selectors;

function getRows(rows, filters) {
  return selectors.getRows({ rows, filters });
}

const Datas = () => {

  const dispatch=useDispatch()
  const col = useSelector((state) =>
    state.colReducer.collumn[0].map((c,indx) => {if(indx ==1 || indx== 3)return{
      ...c,
      ...defaultColumnProperties,
      ...sortColProperties
    }
    else if(indx==0 )return{
      ...c,
      ...defaultColumnProperties,
      ...autoCol
    }
    else return{
      ...c,
      ...defaultColumnProperties,
    }
    
    }
   )
  );

  const ro = useSelector((state) => state.rowReducer.rows[0]);
  const [filters, setFilters] = useState({});
  const [collumn, setCollumn] = useState(col);
  const [rows, setRows] = useState(ro);
  const filteredRows = getRows(rows, filters);
  const [selecteds, setSelected] = useState([]);
  const toolbar = <Toolbar enableFilter />;
  //filter
  const handleFilterChange = (filter) => (filters) => {
    const newFilters = { ...filters };
    if (filter.filterTerm) {
      newFilters[filter.column.key] = filter;
    } else {
      delete newFilters[filter.column.key];
    }
    return newFilters;
  };
  const handleOnClearFilters = () => {
    setFilters({});
  };
  const getValidFilterValues = (columnId) => {
 
    let values = rows.map((r) => r[columnId]);
    return values.filter((item, i, a) => {
      return i === a.indexOf(item);
    });
  
  };
  
//click
const navigate = useNavigate();

  const handleDoubleClick = (e) => {

    dispatch(selected(rows[e]));
    navigate(`/moreInfo/${rows[e]['ENTITY.OBJECT_ID']}`);
  };

  //select
  const onRowsSelected = (rows) => {
    setSelected(selecteds.concat(rows.map((r) => r.rowIdx)));
  };

  const onRowsDeselected = (rows) => {
    let rowIndexes = rows.map((r) => r.rowIdx);
    setSelected(selecteds.filter((i) => rowIndexes.indexOf(i) === -1));
  };
  //sort
  const sortRows = (rows, sortColumn, sortDirection) => {
    const comparer = (a, b) => {
      if (sortDirection === "ASC") {
        return a[sortColumn] > b[sortColumn] ? 1 : -1;
      } else if (sortDirection === "DESC") {
        return a[sortColumn] < b[sortColumn] ? 1 : -1;
      }
    };

    const sortedRows =
      sortDirection === "NONE" ? rows : [...rows].sort(comparer);
    setRows(sortedRows);
  };

  return (
    <div>
<h1>User data:</h1>
      <ReactDataGrid
      minWidth={1300}
      minHeight={450}
        onRowDoubleClick={handleDoubleClick}
        toolbar={toolbar}
        columns={collumn}
        rowGetter={(i) => filteredRows[i]}
        rowsCount={filteredRows.length}
        //sort
        onGridSort={(sortColumn, sortDirection) =>
          sortRows(rows, sortColumn, sortDirection)
        }
        //filter
        onAddFilter={(filter) => setFilters(handleFilterChange(filter))}
        onClearFilters={handleOnClearFilters}
        getValidFilterValues={getValidFilterValues}
        //select
        rowSelection={{
          showCheckbox: true,
          enableShiftSelect: true,
          onRowsSelected: onRowsSelected,
          onRowsDeselected: onRowsDeselected,
          selectBy: {
            indexes: selecteds,
          },
        }}

      />
    </div>
  );
};

export default Datas;
