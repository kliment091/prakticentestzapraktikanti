import React, { useState } from "react";
import Form from "@rjsf/core";
import "bootstrap/dist/css/bootstrap.min.css";
import { connect, useDispatch, useSelector } from "react-redux";
import { update } from "../Actions";
import swal from "sweetalert";
const schema = {

  properties: {
    "ENTITY.NAME": {
      type: "string",
      title: "First Name: ",
    },
    "ENTITY.ADDITIONAL_NAME": {
      type: "string",
      title: "Middle Name: ",
    },
    "ENTITY.SURNAME": {
      type: "string",
      title: "Last Name: ",
    },
    "ENTITY.BIRTH_DATE": {
      type: "string",
      title: "Date of birth: ",
    },
  
  "ENTITY.LOCATION":{
    "type":"string",
    "title":"Location address: "
},
"ENTITY.ADDRESS":{
  "type":"string",
  "title":"Street: "
},
"ENTITY.MOBILE":{
  "type":"string",
  "title":"Phone number 1: "
},"ENTITY.PHONE":{
  "type":"string",
  "title":"Phone number 2: "
},
  },
};
const FormContainer = (props) => {
  const dispatch = useDispatch();

  return (
    <div className="formContainer">
      <div className="innerFrom">
        <Form
          className="formanova "
          formData={props.itemState}
          onSubmit={(e) => {
            swal("Edit Completed!", "Component updated", "success");
            dispatch(update(e.formData))
          }}
          schema={schema}
        />
      </div>
    </div>
  );
};
const mapStateToProps = (state) => {
  const itemState = state.selReducer;
  return {
    itemState,
  };
};

export default connect(mapStateToProps)(FormContainer);
