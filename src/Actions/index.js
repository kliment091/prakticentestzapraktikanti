export const fetch = (load) => {
    return{
        type:'fetch',
        payload:load
    }
}

export const fetchR = (load) => {
    return{
        type:'fetchR',
        payload:load
    }
}



export const selected = (load) => {
    return{
        type:'selected',
        payload:load
    }
}


export const update=(load)=>{
    return{
        type:'update',
        payload:load
    }
}
export const cleanup=()=>{
return{
    type:"cleanup"
}
}